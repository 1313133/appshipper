import React, { useEffect } from 'react';
import { ActivityIndicator, Image } from 'react-native';
import { SafeAreaView } from 'react-native';
import { StyleSheet } from 'react-native';
import { Text, View } from 'react-native';
import { GREEN, MAIN_COLOR, WHITE } from '../asset/color';
import {SkypeIndicator } from 'react-native-indicators'
export const LoadingLabel = () => {
    return(
        <SkypeIndicator animating={true} color={WHITE} size={30}></SkypeIndicator>
    )
}
const ActivityLoading = ({navigation, route}) => {
    
    return (
        <View style={{zIndex: 20,position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', borderWidth: 1, backgroundColor: '#F5FCFF88'}}>
            <SkypeIndicator animating={true} size={40} color={GREEN}></SkypeIndicator>
        </View>
  )
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: MAIN_COLOR,
    }
})
export default ActivityLoading;