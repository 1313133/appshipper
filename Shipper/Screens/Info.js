import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView, Modal, Linking, TextInput} from 'react-native';
import Barcode from '@kichiyaki/react-native-barcode-generator'
import { GRAY, GREEN, WHITE, ORANGE, RED } from '../asset/color';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm from 'react-native-simple-radio-button';
import { ToastAndroid } from 'react-native';
import { useEffect } from 'react';
import { asyncGET, asyncPOST } from '../global/func';
import { getStatus } from '../global/status';
import { launchCamera } from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import ActivityLoading from '../Loading/ActivityLoading';
import { ListItem } from '../global/listcontent';

const Info = ({navigation, route}) => {
    const [twoButton, setTwoButton] = useState(false)
    const [sentsuccess, setSentSuccess] = useState(false)
    const [Sentfail, setSentFail] = useState(false)
    const [confirm, setConfirm] = useState(false)
    const [deny, setDeny] = useState(false)
    const [radioButtons, setRadioButtons] = useState('')
    const {BarcodeValue, ProductID} = route.params;
    const [data,setData] = useState()
    const [status, setStatus] = useState()
    const [imageURI, setImageURI] = useState(null)
    const [showError, setShowError] = useState('')
    const [loading, setLoading] = useState(true)
    const {extraData} = useState(data)
    useEffect(()=>{
        getData();
      },[]);
    const getData = () => {
        ordersData()
    }
    const onPressRadioButton = (value) => {
        ToastAndroid.show(value, ToastAndroid.SHORT)
    }
    const ordersData = () => {
        asyncGET(`api/search/${BarcodeValue}`).then((res) => {
            if(res.Status = 200)
            {
                setData(res)
                console.log(res)
                setLoading(false)
            }
            else{
                if(res.Status == 500)
                {
                  ToastAndroid.showWithGravity('lỗi',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                } else{
                  ToastAndroid.showWithGravity('lỗi data',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                }
              }
        })
    }
    const OpenCamera = () =>{
        ImagePicker.openCamera({
            width: 100,
            height: 100,
            cropping: false,
            mediaType: 'photo',
        }).then(image => {
            console.log('received image');
            console.log(image)
            setImageURI({
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
            })
        }).catch(e=> console.log(e))
    }
    const createFormData = (photo) => {
        const data = new FormData();
        data.append('photo', {
            type: photo.mime,
            uri: photo.uri.replace("file://", "")
        })
        return data
    }
    const onSentSuccess = () =>{
        if(imageURI == null)
        {
            setShowError('Vui lòng chụp ảnh kiện hàng')
        }else{
            ChangeStatus(4, imageURI)
            navigation.navigate('Success',{
            BarcodeValue : BarcodeValue})
            setShowError('')
        }
    }
    const onSentFail = () =>{
        if(imageURI == null)
        {
            setShowError('Vui lòng chụp ảnh kiện hàng')
        }else{
            ChangeStatus(8, imageURI)
            navigation.navigate('Fail',{
                BarcodeValue : BarcodeValue,
                ProductID : ProductID
        })
    }
    }
    const onConfirm = () =>{
        // ChangeStatusWithoutImage(3, 'Đã nhận đơn hàng')
        // setTwoButton(true)
        // setConfirm(false)
        setLoading(true)
        setConfirm(false)
        setTimeout(()=>{
            ChangeStatusWithoutImage(3, 'Đã nhận đơn hàng')
            setTwoButton(true)
            ordersData()
        },2000)
    }
    const onDeny = () =>{
        if(imageURI == null)
        {
            setShowError('Vui lòng chụp ảnh kiện hàng')
        }
        else{   
            ChangeStatus(7, imageURI)
            navigation.navigate('Home')
            setConfirm('')
        }
    }
    const ChangeStatus = async(status, imageURI) => {
        var obj = {
            "Status" : status,
            "note" : JSON.stringify(createFormData(imageURI)),
        }
        asyncPOST(`api/updateStatus/${ProductID}`,obj).then((res)=>{
            if(res.Status = 200)
            {
                console.log(res)
            }
            else{
                ToastAndroid.show('Lỗi',ToastAndroid.TOP)
            }
        })
    }
    const ChangeStatusWithoutImage = async(status, note) => {
        var obj = {
            "Status" : status,
            "note" : note
        }
        asyncPOST(`api/updateStatus/${ProductID}`,obj).then((res)=>{
            if(res.Status = 200)
            {
                console.log(res)
            }
            else{
                ToastAndroid.show('Lỗi',ToastAndroid.TOP)
            }
        })
    }
    const BottomButton = () =>{
        return(
            <View>
                {
                    twoButton ?
                <View>
                    <TouchableOpacity style={styles.buttonStyle}
                        onPress={()=>setSentSuccess(true)}>
                        <Text style={{color: WHITE, fontWeight:'bold'}}>
                            GIAO THÀNH CÔNG
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonStyle2}
                        onPress={()=>setSentFail(true)}>
                        <Text style={{color: WHITE, fontWeight:'bold'}}>
                            GIAO KHÔNG THÀNH CÔNG
                        </Text>
                    </TouchableOpacity>
                </View>
                :
                <View>
                    <TouchableOpacity style={styles.buttonStyle}
                                onPress={()=>{setConfirm(true)}}>
                        <Text style={{color: WHITE, fontWeight:'bold'}}>NHẬN ĐƠN HÀNG</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={()=>setDeny(true)}
                        style={styles.buttonStyle2}>
                        <Text style={{color: WHITE, fontWeight:'bold'}}>TỪ CHỐI NHẬN ĐƠN HÀNG</Text>
                    </TouchableOpacity>
                </View> 
                }  
            </View>
            
        )
    }
    return (
    <SafeAreaView style={styles.container}>
        {
            loading && 
            <ActivityLoading></ActivityLoading>
        }
        <View style={styles.topView}>
            <TouchableOpacity 
                style={{borderWidth: 0, left: 0, position: 'absolute', marginLeft: 15}}
                onPress={()=>{navigation.goBack()}}>
                <Image source={require('../img/arrow.png')} ></Image>
            </TouchableOpacity>
            <View style={{borderWidth: 0,flexDirection: 'row', position: 'absolute', right: 0}}>
                <TouchableOpacity style={{flex: 2, paddingRight: 20}}
                    onPress={()=>navigation.navigate('Order')}>
                    <Image source={require('../img/notivector.png')}></Image>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{fontSize: 20}}>Thông tin đơn hàng</Text>
            </View>
        </View>
        <FlatList
            data={data}
            extraData = {extraData}
            keyExtractor={(item)=>item.id}
            renderItem={(item)=>ListItem(item)}
        ></FlatList>
        <BottomButton></BottomButton>
        <Modal
            visible={sentsuccess}
            transparent={true}
            animationType={'slide'}>
            <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View
                    style={{backgroundColor: WHITE, width: 312, alignItems: 'center', elevation: 12}}>
                    <Image
                        style={{marginTop: 30}}
                        source={require('../img/package.png')}
                    ></Image>
                    <View style={{alignItems: 'center', marginTop: 20, width:'95%', alignSelf: 'center', marginBottom: 10}}>
                        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
                            Xác nhận giao hàng thành công!                        
                        </Text>
                    </View>
                    <View style={{width: '90%',marginBottom: 10, alignSelf: 'center',}}>
                        <Text style={{fontSize: 14, opacity: 0.7, fontWeight: 'bold'}}>Ảnh xác nhận </Text>
                        <View style={{borderWidth: 0.5, width: '100%', marginTop: 10, flexDirection: 'row'}}>
                            <TouchableOpacity style={{alignSelf: 'center', padding: 10, right: 0, position: 'absolute'}} onPress={()=>{OpenCamera()}}>
                                <Image source={require('../img/PicVector.png')}></Image>
                            </TouchableOpacity>
                            <Image source={imageURI} style={{width: 50, height: 50, resizeMode: 'contain', borderWidth: 1, alignSelf: 'center', margin: 10}}></Image>
                        </View>
                    </View>
                    <View View style={{justifyContent: 'center', width: '90%', alignSelf: 'center', marginBottom: 10}}>
                        <Text style={{fontStyle:'italic', color: RED}}>{showError}</Text>
                    </View>
                    <View style={{flexDirection: 'row',marginBottom: 10}}>
                        <TouchableOpacity
                            onPress={()=>{setSentSuccess(false)}}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: '#000000', fontSize: 18}}>
                                    ĐÓNG
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonModalStyle}
                            onPress={()=>{onSentSuccess()}}>
                                <Text 
                                    style={{color: GREEN, fontWeight: 'bold', fontSize: 18}}>
                                    XÁC NHẬN
                                </Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </SafeAreaView>
        </Modal>
        <Modal
            visible={Sentfail}
            transparent={true}
            animationType={'slide'}>
            <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View
                    style={{backgroundColor: WHITE, width: 312, alignItems: 'center', elevation: 12}}>
                    <Image
                        style={{marginTop: 30}}
                        source={require('../img/packagefail.png')}
                    ></Image>
                    <View style={{alignItems: 'center', marginTop: 20, width:'90%', alignSelf: 'center'}}>
                        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
                        Giao hàng không thành công.
                        Xác nhận để hoàn hàng lại?                        
                        </Text>
                    </View>
                    <View style={{marginTop: 20, alignSelf: 'flex-start', marginLeft: 20}}>
                        <Text style={{fontSize: 16, textAlign: 'center', fontWeight: 'bold'}}>
                            Lý do                     
                        </Text>
                    </View>
                    <View style={{width: '90%', marginTop: 10, alignSelf: 'center', marginBottom: 10}}>
                        <RadioForm
                            radio_props={[{
                                label: 'Hàng bị hư hỏng',
                                value: 'Hàng bị hư hỏng'
                            }, {
                                label: 'Khách không nhận hàng',
                                value: 'Khách không nhận hàng'
                            },{
                                label:'Khách hẹn giao lại',
                                value: 'Khách hẹn giao lại'
                            },{
                                label: 'Khác',
                                value: 'Khác'
                            }]}
                            buttonSize={10}
                            buttonColor={'#000000'}
                            onPress={onPressRadioButton}
                            selectedButtonColor={GREEN}
                            selectedLabelColor={GREEN}
                            labelStyle={{fontSize: 16}}>
                       </RadioForm>
                    </View>
                    <View style={{width: '90%',marginBottom: 10, alignSelf: 'center',}}>
                        <Text style={{fontWeight: 'bold', fontSize: 16}}>Ảnh kiện hàng</Text>
                        <View style={{borderWidth: 0.5, width: '100%', marginTop: 10, flexDirection: 'row',}}>
                            <TouchableOpacity style={{alignSelf: 'center', padding: 10, right: 0, position: 'absolute'}} onPress={()=>{OpenCamera()}}>
                                <Image source={require('../img/PicVector.png')}></Image>
                            </TouchableOpacity>
                            <Image source={imageURI} style={{width: 50, height: 50, resizeMode: 'contain', borderWidth: 1, alignSelf: 'center', margin: 10}}></Image>
                        </View>
                    </View>
                    <View View style={{justifyContent: 'center', width: '90%', alignSelf: 'center', marginBottom: 10}}>
                        <Text style={{fontStyle:'italic', color: RED}}>{showError}</Text>
                    </View>
                    <View style={{flexDirection: 'row',marginBottom: 10}}>
                        <TouchableOpacity
                            onPress={()=>{setSentFail(false)}}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: '#000000', fontSize: 18}}>
                                    ĐÓNG
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>onSentFail()}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: GREEN, fontWeight: 'bold', fontSize: 18}}>
                                    XÁC NHẬN
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
        <Modal
            visible={confirm}
            transparent={true}
            animationType={'slide'}>
            <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View
                    style={{backgroundColor: WHITE, width: 312, height: 295, alignItems: 'center', elevation: 12}}>
                    <Image
                        style={{marginTop: 30}}
                        source={require('../img/nonepackage.png')}
                    ></Image>
                    <View style={{alignItems: 'center', marginTop: 20, width:'70%', alignSelf: 'center'}}>
                        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
                            Xác nhận nhận đơn hàng thành công!                        
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',flex: 1}}>
                        <TouchableOpacity
                            onPress={()=>{setConfirm(false)}}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: '#000000', fontSize: 18}}>
                                    ĐÓNG
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonModalStyle}
                            onPress={()=>{onConfirm()}}>
                                <Text 
                                    style={{color: GREEN, fontWeight: 'bold', fontSize: 18}}>
                                    XÁC NHẬN
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
        <Modal
            visible={deny}
            transparent={true}
            animationType={'slide'}>
            <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{backgroundColor: WHITE, width: 312, elevation: 12}}>
                    <View style={{alignItems: 'center', marginTop: 10, width:'70%', alignSelf: 'center'}}>
                        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
                            Từ chối nhận đơn hàng!                        
                        </Text>
                    </View>
                    <Image
                        style={{marginTop: 30, alignSelf: 'center'}}
                        source={require('../img/packagefail.png')}
                    ></Image>
                    <View style={{marginTop: 20, alignSelf: 'flex-start', marginLeft: 20}}>
                        <Text style={{fontSize: 16, textAlign: 'center', fontWeight: 'bold'}}>
                            Lý do                     
                        </Text>
                    </View>
                    <View style={{width: '90%', marginTop: 10, alignSelf: 'center'}}>
                        <RadioForm
                            radio_props={[{
                                label: 'Hàng bị hư hỏng',
                                value: 'Hàng bị hư hỏng'
                            }, {
                                label: 'Khách không nhận hàng',
                                value: 'Khách không nhận hàng'
                            },{
                                label:'Khách hẹn giao lại',
                                value: 'Khách hẹn giao lại'
                            },{
                                label: 'Khác',
                                value: 'Khác'
                            }]}
                            buttonSize={10}
                            buttonColor={'#000000'}
                            onPress={onPressRadioButton}
                            selectedButtonColor={GREEN}
                            selectedLabelColor={GREEN}
                            labelStyle={{fontSize: 16}}>
                       </RadioForm>
                    </View>
                    <View style={{width: '90%',marginBottom: 10, alignSelf: 'center',}}>
                        <Text style={{fontWeight: 'bold', fontSize: 16}}>Ảnh kiện hàng</Text>
                        <View style={{borderWidth: 0.5, width: '100%', marginTop: 10, flexDirection: 'row',}}>
                            <TouchableOpacity style={{alignSelf: 'center', padding: 10, right: 0, position: 'absolute'}} onPress={()=>{OpenCamera()}}>
                                <Image source={require('../img/PicVector.png')}></Image>
                            </TouchableOpacity>
                            <Image source={imageURI} style={{width: 50, height: 50, resizeMode: 'contain', borderWidth: 1, alignSelf: 'center', margin: 10}}></Image>
                        </View>
                    </View>
                    <View View style={{justifyContent: 'center', width: '90%', alignSelf: 'center', marginBottom: 10}}>
                        <Text style={{fontStyle:'italic', color: RED}}>{showError}</Text>
                    </View>
                    <View style={{flexDirection: 'row',marginBottom: 10}}>
                        <TouchableOpacity
                            onPress={()=>{setDeny(false)}}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: '#000000', fontSize: 18}}>
                                    ĐÓNG
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: GREEN, fontWeight: 'bold', fontSize: 18}}
                                    onPress={()=>onDeny()}>
                                    XÁC NHẬN
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:WHITE, flexDirection:'column'
    },
    topView:{
        borderWidth: 0,
        width:'100%',
        height: '9%',
        elevation: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        
    },
    buttonStyle:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: GREEN,
        justifyContent: 'center',
        marginBottom: 15
    },  
    buttonModalStyle:{
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: WHITE,
        justifyContent: 'flex-end',
        borderWidth: 0,
        flex: 1
    },
    buttonStyle2:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: ORANGE,
        justifyContent: 'center',
        marginBottom: 15
    },  
})
export default Info;