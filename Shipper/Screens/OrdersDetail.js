import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView, Modal, Linking} from 'react-native';
import Barcode from '@kichiyaki/react-native-barcode-generator'
import { GRAY, GREEN, WHITE, ORANGE } from '../asset/color';
import DashedLine from 'react-native-dashed-line';
import { data } from '../asset/datasample';
import { asyncGET } from '../global/func';
import { useEffect } from 'react';
import { getStatus, status1, status2, status3, status4, status5, status6, status7, status8 } from '../global/status';
import ActivityLoading from '../Loading/ActivityLoading';
import { ListItem } from '../global/listcontent';
const OrdersDetail = ({navigation, route}) => {
    const {OrderID} = route.params;
    const [twoButton, setTwoButton] = useState(false)
    const [data, setData] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(()=>{
        getData();
      },[])
    const getData = () => {
        ordersData()
    }
    
    
    const ordersData = () => {
        asyncGET('api/getlistorder').then((res) => {
            if(res.Status = 200)
            {
                var value = (res).filter(function(res){
                    return res.id == OrderID
                })
                setData(value)
                setLoading(false)
                console.log(value)
            }
            else{
                if(res.Status == 500)
                {
                  ToastAndroid.showWithGravity('lỗi',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                } else{
                  ToastAndroid.showWithGravity('lỗi data',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                }
              }
        })
    }
    
    // const StatusItem = ({item}) => {
    //     return(
    //         <View>
    //             <View style={styles.titleContent}>
    //                 <Text style={{fontSize: 14, color: GRAY, fontWeight: 'bold'}}>Lịch sử trạng thái</Text>
    //                 {/* <Text>{OrderID}</Text> */}
    //             </View>
    //             <View style={{padding: 20, flexDirection: 'column'}}> 
    //                 <View style={{flexDirection: 'row', alignItems: 'center'}}>{/*trạng thái*/}
    //                     <Image source={require('../img/loadingicon.png')}></Image>
    //                     <View style={{paddingLeft: 10}}>
    //                         <Text style={{fontWeight: 'bold', fontSize: 15}}>Đang xử lý</Text>
    //                     </View>
    //                 </View>
    //                 <View style={{borderStyle:'dashed', borderLeftWidth: 0.2, borderRadius: 1, marginLeft: 7, padding: 10, borderColor: GREEN}}>
    //                     <View style={{paddingLeft: 10}}>
    //                         <View style={{flexDirection: 'row', marginBottom: 5}}>
    //                             <Text>13:05:13  - 12/05/2021:</Text>
    //                             <Text> Đã đóng gói</Text>
    //                         </View>
    //                         <View style={{flexDirection: 'row',  marginBottom: 5}}>
    //                             <Text>13:05:13  - 12/05/2021:</Text>
    //                             <Text> Đang đóng gói</Text>
    //                         </View>
    //                     </View>
    //                 </View>
    //             </View>
    //         </View>
    //     )
    // }
    
    
    return (
    <SafeAreaView style={styles.container}>
        {
            loading && 
            <ActivityLoading></ActivityLoading>
        }
        <View style={styles.topView}>
            <View style={{borderWidth: 0,flexDirection: 'row', position: 'absolute', right: 0}}>
                <TouchableOpacity style={{flex: 2, paddingRight: 20}}
                    onPress={()=>navigation.navigate('Order')}>
                    <Image source={require('../img/notivector.png')}></Image>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={{borderWidth: 0, left: 0, position: 'absolute', marginLeft: 20, marginTop: 15}}
                onPress={()=>{navigation.goBack()}}>
            <Image source={require('../img/arrow.png')} style={{tintColor: '#000000'}}></Image>
            </TouchableOpacity>
            <View>
                <Text style={{fontSize: 20}}>Thông tin đơn hàng</Text>
            </View>
        </View>
        <FlatList
            data={data}
            renderItem={(item)=>ListItem(item)}
        ></FlatList>
        <View>
            <TouchableOpacity style={styles.buttonStyle}
                        onPress={()=>{navigation.navigate('Home')}}>
                <Text style={{color: WHITE, fontWeight:'bold'}}>VỀ TRANG CHỦ</Text>
                <Image source={require('../img/homevector.png')} style={{marginLeft: 10}}></Image>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:WHITE, flexDirection:'column'
    },
    topView:{
        borderWidth: 0,
        width:'100%',
        height: '9%',
        elevation: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },  
    buttonModalStyle:{
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: WHITE,
        justifyContent: 'flex-end',
        borderWidth: 0,
        flex: 1
    },
    buttonStyle:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: GREEN,
        justifyContent: 'center',
        marginBottom: 15,
        flexDirection: 'row'
    },  
})
export default OrdersDetail;