import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView, Modal} from 'react-native';
import Barcode from '@kichiyaki/react-native-barcode-generator'
import { GRAY, GREEN, WHITE, ORANGE, BLUE } from '../asset/color';
import DashedLine from 'react-native-dashed-line';
import { asyncGET, asyncPOST } from '../global/func';
import { ToastAndroid } from 'react-native';
import { getStatus } from '../global/status';
import ActivityLoading from '../Loading/ActivityLoading';
import { ListItem } from '../global/listcontent';
const InfoFail = ({navigation, route}) => {
    const [success, setSuccess] = useState(false)
    const [fail, setFail] = useState(false)
    const {BarcodeValue, ProductID} = route.params;
    const [loading, setLoading] = useState(true)
    const [data,setData] = useState()
    
    useEffect(()=>{
        getData();
      },[]);
    
      const getData = () => {
        ordersData()
    }
    
    const ordersData = () => {
        asyncGET(`api/search/${BarcodeValue}`).then((res) => {
            if(res.Status = 200)
            {   
                setLoading(false)
                setData(res)
                console.log(res)
            }
            else{
                if(res.Status == 500)
                {
                  ToastAndroid.showWithGravity('lỗi',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                } else{
                  ToastAndroid.showWithGravity('lỗi data',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                }
              }
        })
    }
    const onConfirm = () => {
        ChangeStatus(8, 'Hoàn trả hàng thành công')
        navigation.navigate('Success',{
            BarcodeValue : BarcodeValue
        })
    }
    const ChangeStatus = async(status, note) => {
        var obj = {
            "Status" : status,
            'note' : note
        }
        asyncPOST(`api/updateStatus/${ProductID}`,obj).then((res)=>{
            if(res.Status = 200)
            {
                console.log(res)
            }
            else{
                ToastAndroid.show('Lỗi',ToastAndroid.TOP)
            }
        })
    }
    
    return (
    <SafeAreaView style={styles.container}>
        {
            loading && 
            <ActivityLoading></ActivityLoading>
        }
        <View style={styles.topView}>
            <View style={{borderWidth: 0,flexDirection: 'row', position: 'absolute', right: 0}}>
                <TouchableOpacity style={{flex: 2, paddingRight: 20}}
                    onPress={()=>navigation.navigate('Order')}>
                    <Image source={require('../img/notivector.png')}></Image>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{fontSize: 20}}>Thông tin đơn hàng</Text>
            </View>
        </View>
        <FlatList
            data={data}
            renderItem={(item)=>ListItem(item)}
        ></FlatList>
        <Modal
            visible={success}
            transparent={true}
            animationType={'slide'}>
            <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View
                    style={{backgroundColor: WHITE, width: 312, height: 295, alignItems: 'center', elevation: 12}}>
                    <Image
                        style={{marginTop: 30}}
                        source={require('../img/packagereturn.png')}
                    ></Image>
                    <View style={{alignItems: 'center', marginTop: 20, width:'70%', alignSelf: 'center'}}>
                        <Text style={{fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>
                            Xác nhận Hoàn hàng thành công!                       
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',flex: 1}}>
                        <TouchableOpacity
                            onPress={()=>{setSuccess(false)}}
                            style={styles.buttonModalStyle}>
                                <Text 
                                    style={{color: '#000000', fontSize: 18}}>
                                    ĐÓNG
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonModalStyle}
                            onPress={()=>{onConfirm()}}>
                                <Text 
                                    style={{color: GREEN, fontWeight: 'bold', fontSize: 18}}>
                                    XÁC NHẬN
                                </Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </SafeAreaView>
        </Modal>
        <View>
            <TouchableOpacity style={styles.buttonStyle}
                onPress={()=>{setSuccess(true)}}>
                <Text style={{color: WHITE, fontWeight:'bold'}}>HOÀN HÀNG THÀNH CÔNG</Text>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:WHITE, flexDirection:'column'
    },
    topView:{
        borderWidth: 0,
        width:'100%',
        height: '9%',
        elevation: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        
    },
    buttonStyle:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: BLUE,
        justifyContent: 'center',
        marginBottom: 15
    },  
    buttonModalStyle:{
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: WHITE,
        justifyContent: 'flex-end',
        borderWidth: 0,
        flex: 1
    },
})
export default InfoFail;