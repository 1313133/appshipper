import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView, Modal} from 'react-native';
import Barcode from '@kichiyaki/react-native-barcode-generator'
import { GRAY, GREEN, WHITE, ORANGE } from '../asset/color';
import DashedLine from 'react-native-dashed-line';
import { data } from '../asset/datasample';
import { useEffect } from 'react';
import { asyncGET } from '../global/func';
import { ToastAndroid } from 'react-native';
import { getStatus } from '../global/status';
import ActivityLoading from '../Loading/ActivityLoading';
import { ListItem } from '../global/listcontent';
const InfoSuccess = ({navigation,route}) => {
    const [twoButton, setTwoButton] = useState(false)
    const [data,setData] = useState()
    const {BarcodeValue} = route.params;
    const [loading, setLoading] = useState(true)

    useEffect(()=>{
        getData();
      },[]);
    const getData = () => {
        ordersData()
    }
    const ordersData = () => {
        asyncGET(`api/search/${BarcodeValue}`).then((res) => {
            if(res.Status = 200)
            {
                setLoading(false)
                setData(res)
                console.log(res)
            }
            else{
                if(res.Status == 500)
                {
                  ToastAndroid.showWithGravity('lỗi',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                } else{
                  ToastAndroid.showWithGravity('lỗi data',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
                  console.log(res)
                }
              }
        })
    }
    
    return (
    <SafeAreaView style={styles.container}>
        {
            loading && 
            <ActivityLoading></ActivityLoading>
        }
        <View style={styles.topView}>
            <View style={{borderWidth: 0,flexDirection: 'row', position: 'absolute', right: 0}}>
                <TouchableOpacity style={{flex: 2, paddingRight: 20}}
                    onPress={()=>navigation.navigate('Order')}>
                    <Image source={require('../img/notivector.png')}></Image>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{fontSize: 20}}>Thông tin đơn hàng</Text>
            </View>
        </View>
        <FlatList
            data={data}
            renderItem={(item)=>ListItem(item)}
        ></FlatList>
        <View>
            <TouchableOpacity style={styles.buttonStyle}
                        onPress={()=>{navigation.navigate('Home')}}>
                <Text style={{color: WHITE, fontWeight:'bold'}}>VỀ TRANG CHỦ</Text>
                <Image source={require('../img/homevector.png')} style={{marginLeft: 10}}></Image>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:WHITE, flexDirection:'column'
    },
    topView:{
        borderWidth: 0,
        width:'100%',
        height: '9%',
        elevation: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        
    },
    titleContent:{
        paddingLeft: 20,
        backgroundColor: '#F2F2F2',
        width: '100%',
        height:40,
        justifyContent: 'center'
    }, 
    textTitle:{
        color: GRAY
    },
    buttonStyle:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: GREEN,
        justifyContent: 'center',
        marginBottom: 15,
        flexDirection: 'row'
    },  
    buttonModalStyle:{
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: WHITE,
        justifyContent: 'flex-end',
        borderWidth: 0,
        flex: 1
    },
})
export default InfoSuccess;