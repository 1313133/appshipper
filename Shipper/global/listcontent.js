import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView, Modal, Linking} from 'react-native';
import Barcode from '@kichiyaki/react-native-barcode-generator'
import { GRAY, GREEN, WHITE } from '../asset/color';
import { getStatus, status1, status2, status3, status4, status5, status6, status7, status8 } from '../global/status';
import { getPayment } from './payment';

const BarcodeItem = (item) =>{
    return(
        <Barcode 
        style={{marginTop: 20}}
        format={'CODE128'} value={item.code} text={item.code}
        textStyle={{marginTop: 14, fontSize: 20, fontWeight: 'bold', marginBottom: 10}}/>
    )
}
const SentInfo = (item) =>{
    return(
        <View>
            <View style={styles.titleContent}>
                <Text style={{fontSize: 14, color: GRAY, fontWeight: 'bold'}}>Thông tin người gửi</Text>
            </View>
            <View style={{flexDirection: 'column', }}>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1}}>
                        <Text style={styles.textTitle}>Số điện thoại</Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <TouchableOpacity style={{flexDirection: 'row', backgroundColor: GREEN,alignItems: 'center', paddingLeft: 20, paddingRight: 10, padding: 2, borderRadius: 4}}
                                        onPress={()=>{Linking.openURL(`tel:${item.sendermobile}`)}}>
                            <Text style={{fontWeight: 'bold', color: WHITE, marginRight: 10}}>{item.sendermobile}</Text>
                            <Image source={require('../img/phongvector.png')}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1,}}>
                        <Text style={styles.textTitle}>Họ và tên </Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <Text>{item.sender}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'column',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, marginBottom: 5 }}>
                        <Text style={styles.textTitle}>Địa chỉ</Text>
                    </View>
                    <View style={{borderWidth: 0,  alignItems: 'flex-start', flexDirection: 'row'}}>
                        <View style={{width: '90%'}}>
                            <Text>{item.senderaddress}</Text>
                        </View>
                        <View style={{alignItems: 'flex-end', flex: 1, borderWidth: 0, alignSelf: 'center'}}>
                            <Image source={require('../img/placevector.png')}></Image>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}
const ProductInfo = (item) =>{
    return(
    <View>
        <View style={styles.titleContent}>
            <Text style={{fontSize: 14, color: GRAY, fontWeight: 'bold'}}>Thông tin kiện hàng</Text>
        </View>
        <View style={{flexDirection: 'column', }}>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1}}>
                    <Text style={styles.textTitle}>Trạng thái</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{getStatus(item)}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Tổng tiền thu</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.price}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Thông tin kiện hàng</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.description}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Kích thước kiện hàng</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.SizeOrder}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Ghi Chú</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.isDangerous}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Bên thanh toán</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{getPayment(item)}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Phí thu hộ (COD)</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.cod}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row',borderBottomWidth: 0, padding: 20, }}>
                <View style={{borderWidth: 0, flex: 1,}}>
                    <Text style={styles.textTitle}>Hình thức vận chuyển</Text>
                </View>
                <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                    <Text>{item.Methodtrans}</Text>
                </View>
            </View>
        </View>
    </View>
    )
}
const RecieveInfo = (item) =>{
    return(
        <View>
            <View style={styles.titleContent}>
                <Text style={{fontSize: 14, color: GRAY, fontWeight: 'bold'}}>Thông tin người gửi</Text>
            </View>
            <View style={{flexDirection: 'column', }}>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1}}>
                        <Text style={styles.textTitle}>Số điện thoại</Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <TouchableOpacity style={{flexDirection: 'row', backgroundColor: GREEN,alignItems: 'center', paddingLeft: 20, paddingRight: 10, padding: 2, borderRadius: 4}}
                                        onPress={()=>{Linking.openURL(`tel:${item.customermobile}`)}}>
                            <Text style={{fontWeight: 'bold', color: WHITE, marginRight: 10}}>{item.customermobile}</Text>
                            <Image source={require('../img/phongvector.png')}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1,}}>
                        <Text style={styles.textTitle}>Họ và tên </Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <Text>{item.customer}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'column',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, marginBottom: 5 }}>
                        <Text style={styles.textTitle}>Địa chỉ</Text>
                    </View>
                    <View style={{borderWidth: 0,  alignItems: 'flex-start', flexDirection: 'row',}}>
                        <View style={{width: '90%'}}>
                            <Text>{item.customeraddress}</Text>
                        </View>
                        <View style={{alignItems: 'flex-end', flex: 1, borderWidth: 0, alignSelf: 'center'}}>
                            <Image source={require('../img/placevector.png')}></Image>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}
const StockInfo = (item) =>{
    return(
        <View>
            <View style={styles.titleContent}>
                <Text style={{fontSize: 14, color: GRAY, fontWeight: 'bold'}}>Thông tin kho hàng</Text>
            </View>
            <View style={{flexDirection: 'column', }}>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1}}>
                        <Text style={styles.textTitle}>Số điện thoại</Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <TouchableOpacity 
                        style={{flexDirection: 'row', backgroundColor: GREEN,alignItems: 'center', paddingLeft: 20, paddingRight: 10, padding: 2, borderRadius: 4}}
                        onPress={()=>{Linking.openURL(`tel:${item.stockmobile}`)}}>
                            <Text style={{fontWeight: 'bold', color: WHITE, marginRight: 10}}>{item.stockmobile}</Text>
                            <Image source={require('../img/phongvector.png')}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection: 'row',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, flex: 1,}}>
                        <Text style={styles.textTitle}>Họ và tên </Text>
                    </View>
                    <View style={{borderWidth: 0, flex: 1, alignItems: 'flex-end'}}>
                        <Text>{item.Stockname}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'column',borderBottomWidth: 0.2, padding: 20, }}>
                    <View style={{borderWidth: 0, marginBottom: 5 }}>
                        <Text style={styles.textTitle}>Địa chỉ</Text>
                    </View>
                    <View style={{borderWidth: 0,  alignItems: 'flex-start', flexDirection: 'row',}}>
                        <View style={{width: '90%'}}>
                            <Text>{item.addressstock}</Text>
                        </View>
                        <View style={{alignItems: 'flex-end', flex: 1, borderWidth: 0, alignSelf: 'center'}}>
                            <Image source={require('../img/placevector.png')}></Image>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}
// const BottomButton = (props) =>{
//     return(
//         <View>
//             <TouchableOpacity style={styles.buttonStyle}
//                         onPress={()=>{navigation.navigate('Home')}}>
//                 <Text style={{color: WHITE, fontWeight:'bold'}}>VỀ TRANG CHỦ</Text>
//                 <Image source={require('../img/homevector.png')} style={{marginLeft: 10}}></Image>
//             </TouchableOpacity>
//         </View>
        
//     )
// }
export const ListItem = ({item, props}) => {
    return(
        <View>
            {BarcodeItem(item)}
            {/* {StatusItem(item)} */}
            {SentInfo(item)}
            {ProductInfo(item)}
            {RecieveInfo(item)}
            {StockInfo(item)}
            {/* {BottomButton()} */}
        </View>
    )
}
const styles = StyleSheet.create({
    titleContent:{
        paddingLeft: 20,
        backgroundColor: '#F2F2F2',
        width: '100%',
        height:40,
        justifyContent: 'center'
    }, 
    textTitle:{
        color: GRAY
    },
    buttonStyle:{
        height: 50,
        width:'90%',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: GREEN,
        justifyContent: 'center',
        marginBottom: 15,
        flexDirection: 'row'
    },  
})